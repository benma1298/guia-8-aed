
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <chrono>
#include "programa.h"

// Se definen los limites inferior y superior.
#define limitesup 900000
#define limiteinf 1

using namespace std;

//Funcion que imprime el arreglo de datos aleatorios.
void imprimir(int vector[], int num){
    for(int i = 0; i < num; i++){
        cout << "a[" << i << "] = " << vector[i] << "\t";
    }
    cout << endl;
}

//Funcion main
int main(int argc, char* argv[]){
    system("clear");
    int num = 0;
    int dato = 0;
    bool imprime;
    string car;

    // Se llama a la libreria chrono para calcular el tiempo de ejecucion.
    chrono::duration<float> tSelec, tQuick;

    // Se llama a la clase programa.
    programa *Programa;

    // Se reciben los parametros necesarios para el programa.
    if(argc == 3){
        num = atoi(argv[1]);
        car = argv[2];
        if(num > limitesup || num < 2){
            cout << "Dato Erroneo, el numero debe estar entre 2 y 900000" << endl;
            return 1;
        }
    }else{
        cout << "Se deben ingresar 2 parametros por terminal." << endl;
        cout << "Parametro 1: (N = cantidad de numeros aleatorios)" << endl;
        cout << "Parametro 2: Si desea mostrar el contenido de los vectores (s) o no (n)" << endl;
        return 1;
    }
    int arreglo[num];
    int selection[num];
    int quicksort[num];
    srand(time(NULL));

    // Se generan los numeros aleatorios del arreglo inicial.
    for(int i = 0; i < num; i++){
        dato = (rand()%limitesup)+limiteinf;
        arreglo[i] = dato;
    }
    
    // Se declaran los mensajes de la terminal junto con imprimir el arreglo inicial.
    cout << "Guia N8 Metodos de Ordenamiento Interno:" << endl;
    cout << endl;
    cout << "Arreglo inicial, vectores aleatorios: " << endl;
    imprimir(arreglo, num);

    // Se generan dos nuevos arreglos para evitar errores en el funcionamiento del programa. 
    for(int i = 0; i < num; i++){
        selection[i] = arreglo[i];
        quicksort[i] = arreglo[i];
    }
    imprime = car == "s";

    // Calculo del tiempo, Metodo Selection.
    auto tInicial = chrono::high_resolution_clock::now();
    Programa->onSelection(selection, num);
    auto tFinal = chrono::high_resolution_clock::now();
    tSelec = tFinal - tInicial;

    // Calculo del tiempo, Metodo Quicksort.
    tInicial = chrono::high_resolution_clock::now();
    Programa->onQuicksort(quicksort, num);
    tFinal = chrono::high_resolution_clock::now();
    tQuick = tFinal - tInicial;

    // Se distribuyen los datos en la terminal de forma optima.
    cout << endl;
    cout << "Metodo:       || Tiempo: " << endl;
    cout << "........................................" << endl;
    cout << "Selection:    || " << tSelec.count() << " milisegundos." << endl << endl;
    cout << "Quicksort:    || " << tQuick.count() << " milisegundos." << endl << endl;
    cout << endl;

    // Se imprimen los arreglos resultantes de ambos metodos.
    if(imprime){
        cout << "- Metodo Selection: ";
        Programa->imprimeVector(selection, num);

        cout << "- Metodo Quicksort: ";
        Programa->imprimeVector(quicksort, num);
    }
    return 0;
}
