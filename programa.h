#ifndef PROGRAMA_H
#define PROGRAMA_H
#include <iostream>

using namespace std;

class programa{
    private:

    public:
    programa();

    // Funcion que imprime el vector en la terminal.
    void imprimeVector(int vector[], int num);

    // Funcion de ordenamiento; algoritmo selection.c
    void onSelection(int vector[], int num);

    // Funcion de ordenamiento; algoritmo quicksort.c
    void onQuicksort(int vector[], int num);
};

#endif
