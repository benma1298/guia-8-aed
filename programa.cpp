
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include "programa.h"

using namespace std;

// Constructor de la clase programa.cpp
programa::programa(){

}

// Funcion que imprime el vector en cuestion.
void programa::imprimeVector(int vector[], int num){
    for(int i = 0; i < num; i++){
        cout << "a[" << i << "] = " << vector[i] << "\t";
    }
    cout << endl << endl;
}

// Funcion de la ejecucion del metodo selection.c
void programa::onSelection(int vector[], int num){

    // Se declaran las variables que se usaran en selection.c
    int k, min;
    for(int i = 0; i <= num-2; i++){
        min = vector[i];
        k = i;

        for(int j = i+1; j <= num-1; j++){
            if(vector[j] < min){
                min = vector[j];
                k = j;
            }
        }
        vector[k] = vector[i];
        vector[i] = min;
    }
}
// Funcion que inicia el metodo Quicksort.
void iniciarQuicksort(int vector[], int ini, int fin, int &pos){

    // Se declaran las variables a usar.
    int aux, der, izq;
    bool band = true;

    // Se otorgan los valores a las variables.
    izq = ini;
    der = fin;
    pos = ini;
    while(band){
        while((vector[pos] <= vector[der]) && (pos != der)){
            der = der - 1;
        }
        if(pos == der){
            band = false;
        }else{
            aux = vector[pos];
            vector[pos] = vector[der];
            vector[der] = aux;
            pos = der;

            while((vector[pos] >= vector[izq]) && (pos != izq)){
                izq = izq + 1;
            }
            if(pos == izq){
                band = false;
            }else{
                aux = vector[pos];
                vector[pos] = vector[izq];
                vector[izq] = aux;
                pos = izq;
            }
        }
    }
}

// Funcion de la ejecucion del metodo quicksort.c
void programa::onQuicksort(int vector[], int num){

    // Se declaran las variables a usar en quicksort.c
    int tope, ini, fin, pos;
    int pilamenor[num];
    int pilamayor[num];
    tope = 0;
    pilamenor[tope] = 0;
    pilamayor[tope] = num-1;

    while(tope >= 0){
        ini = pilamenor[tope];
        fin = pilamayor[tope];
        tope = tope - 1;

        iniciarQuicksort(vector, ini, fin, pos);
        
        if(ini < (pos - 1)){
            tope = tope + 1;
            pilamenor[tope] = ini;
            pilamayor[tope] = pos - 1;
        }
        if(fin > (pos + 1)){
            tope = tope + 1;
            pilamenor[tope] = pos + 1;
            pilamayor[tope] = fin;
        }
    }
}