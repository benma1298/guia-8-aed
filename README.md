# Guía 8 AED

Guía N8 Algoritmos y Estructura de Datos.
Unidad 3
Tema: Métodos de Ordenamiento Interno.
Autor: Benjamín Martín Albornoz.
Fecha: 29 de Noviembre, 2021.

El presente programa es capaz de ordenar un mismo conjunto de elementos aleatorios utilizando los algortimos de:

    - Selección: Método directo O(n^2))

    - Quicksort: Método logarítmico O(n × logn))

Entregando en conjunto el tiempo, en milisegundos, que demoran ambos metodos en ordenar el mismo conjunto de datos.
El programa recibe como parametros de entrada los siguientes datos:

    - El valor de N (el cual debe ser positivo), que corresponde a la cantidad total de elementos a ordenar.

    - Parametro VER, correspomdiente a las letras (s) para mostrar el contenido de los vectores y (n) para NO hacerlo.

Los pasos para iniciar este programa son:

    1.- Abrir la terminal en el directorio que posee el programa.

    2.- Ejecutar el comando make para su compilacion.

    3.- EJecutar el comando del nombre del programa (main) junto con los 2 parametros N y VER

    Ejemplo: "./main 10 s", siendo 10 la cantidad de total de elementos y s, la decision de mostrar el contenido.
    

